package edu.kau.fcit.cpit252.observers;

public abstract class Observer {
    private String receiver;
    List<Observer> observers = new ArrayList<Observer>();

    public Observer(String receiver) {
        this.receiver = receiver;
    }
    public abstract void notify(String message);

    @verride
    public void subscribe(Observer o){
        observers.add(o);
    }
    @verride
    public void unsubscribe(Observer o){
        observers.remove(o);
    }
    @verride
    public void notifyUpdate(Observer m){
        for(Observer o : observers)
            o.notify(m);
    }
}
